package com.example.demo.convertors;

import org.hl7.fhir.dstu3.model.BooleanType;
import org.springframework.core.convert.converter.Converter;

public class BooleanTypeConverter {

  public static class BooleanTypeReadConverter implements Converter<Boolean, BooleanType> {

    @Override
    public BooleanType convert(Boolean aBoolean) {
      BooleanType booleanType = new BooleanType();
      booleanType.setValue(aBoolean);
      return booleanType;
    }
  }

  public static class BooleanTypeWriteConverter implements Converter<BooleanType, Boolean> {

    @Override
    public Boolean convert(BooleanType booleanType) {
      return booleanType.booleanValue();
    }
  }
}
