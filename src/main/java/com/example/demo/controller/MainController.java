package com.example.demo.controller;

import com.example.demo.repositories.PatientRepository;
import org.hl7.fhir.dstu3.model.Patient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/main")
public class MainController {

  private final PatientRepository patientRepository;

  public MainController(PatientRepository patientRepository) {
    this.patientRepository = patientRepository;
  }

  @GetMapping
  public Iterable<Patient> getPatient() {
    return patientRepository.findAll();
  }

}
