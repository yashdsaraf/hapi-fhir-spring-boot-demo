package com.example.demo.repositories;

import org.hl7.fhir.dstu3.model.Patient;
import org.springframework.data.repository.CrudRepository;

public interface PatientRepository extends CrudRepository<Patient, String> {
}
